# gitlab-ux-userscript

- autoredirect to issues on generic projects like `INBOX`, `OKR`, `OPS`
- switch default choice on issues from `Comment` to `Start discussion`

## Installation

1. install tampermonkey in chrome from [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)
1. install gitlab-ux-userscript in chrome from [gitlab-ux-userscript](https://gitlab.com/sevcik.tk/gitlab-ux-userscript/raw/master/gitlab-ux.user.js)
